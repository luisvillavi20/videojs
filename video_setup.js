videojs('video', {
    controls: true,
    techOrder:  ["youtube"],
    playbackRates: [0.5, 1, 1.5, 2],
   
    plugins: {
        videoJsResolutionSwitcher: {
          dynamicLabel: true,
          default: 'low'
            
        }
    },
    width: 1000,
    sources: [{ "type": "video/youtube", "src": "https://www.youtube.com/watch?v=iD_MyDbP_ZE"}],
    
}, function(){
    var player = this;
    player.on('resolutionchange', function(){
        console.info('Source changed')
    })
});
